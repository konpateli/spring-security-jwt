package com.example.demo.service;

import com.example.demo.request.SigninRequest;
import com.example.demo.response.JwtAuthenticationResponse;
import com.example.demo.request.SignUpRequest;

public interface AuthenticationService {
    JwtAuthenticationResponse signup(SignUpRequest request);

    JwtAuthenticationResponse signin(SigninRequest request);
}
